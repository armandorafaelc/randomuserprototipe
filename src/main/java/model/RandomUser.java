package model;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class RandomUser {

	public Result getUser() {
		Client clientRes = new Client().create();
		WebResource wr = clientRes.resource("https://randomuser.me/api/");

		// Get From Json to OBJ
		//System.out.println("Printei antes \n");
		String json = wr.get(String.class);
		//System.out.println(json);
		//System.out.println("Printei depois \n");

		Gson gson = new Gson();

		Random random = new Random();
		Result resultUsuario = new Result();
		random = gson.fromJson(json, Random.class);

		for (Result result : random.getResults()) {
			resultUsuario = result;
		}
		if (resultUsuario == null) {
			return null;
		} else {
			return resultUsuario;
		}

	}
}